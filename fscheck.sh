#!/bin/bash

getprop() {
    fileSizeInBytes=$(stat -c%s "$FILENAME")
    if [ $fileSizeInBytes -le 1000 ]; then
        x="$fileSizeInBytes"
        fileSize="${x} bytes"
    else
        x=$((fileSizeInBytes / 1000))
        fileSize="${x} kilobytes"
    fi
    
    fetchWordCount=$(wc -w "$FILENAME")
    # Set space as the delimiter
    IFS=' '
    #Read the split words into an array based on space delimiter
    read -a strarr1 <<< "$fetchWordCount"
    wordCount=${strarr1[0]}

    fetchLastModified=$(stat -c%y "$FILENAME")
    # Set space as the delimiter
    IFS='.'
    #Read the split words into an array based on space delimiter
    read -a strarr2 <<< "$fetchLastModified"
    lastModified=${strarr2[0]}
    
    echo "The file $FILENAME contains $wordCount words is $fileSize in size and was last modified $lastModified"
}

read -p 'Enter file name: ' FILENAME
getprop "$FILENAME"
exit 1