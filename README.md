# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Bash Practice (problem details given below)

### Write a script that provide the file details.
### Details:
* Write a script that retrieves the following information about a file:  
a. Its size in kilobytes  
b. The number of words it contains  
c. The date/time it was last modified
### Details
*	Call the script fscheck.sh
*	All code involved in retrieving this information from a specified file is to be contained within a function named getprop()
*	Your script will prompt the user for a file name to check, which will then be handed to the getprop() function, i.e. getprop [file_name]
*	The getprop() function’s output is to be a message to terminal that states “The file [file_name] contains [word_count] words and is [file_size]K in size and was last modified [last_modified_date]”
*	The last modified date is to be formatted as dd-mm-yy hh-mm-ss, e.g. 16-07-2020 09:59:44
*	Position the getprop() function within the fscheck.sh file and not in a separate file
*	When your tutor runs your script, a file containing standard textual content will be used (could be any file type – do not hard-code). The file the tutor uses will be located in the same directory as your script so there is no need 
